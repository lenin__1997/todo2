import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ListAddComponent } from './list-add/list-add.component';
import { ListBodyComponent } from './list-body/list-body.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListAddComponent,
    ListBodyComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
